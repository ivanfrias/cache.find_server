class ChangeCacheTableStructure < ActiveRecord::Migration
  def up
    create_table :locations do |t|
      t.string :lat
      t.string :lon

      t.timestamps
    end
    
    
    create_table :regions do |t|
      t.string :state
      t.string :country
      t.string :county
      t.string :city
      t.string :postal
      
      t.timestamps
    end
    
    change_table :caches do |t|
      t.belongs_to :location
      t.belongs_to :region
      
      t.string :name
      t.string :status
      t.string :terrain
      t.string :oxcode
      t.remove :altitude
      t.remove :latitude
      t.remove :longitude
      t.remove :title
    end
  end

  def down
    drop_table :locations
    drop_table :regions
    
    remove_column :caches, :location
    remove_column :caches, :region
    remove_column :caches, :name
    remove_column :caches, :status
    remove_column :caches, :terrain
    remove_column :caches, :oxcode
    add_column :caches, :altitude, :string
    add_column :caches, :latitude, :string
    add_column :caches, :longitude, :string
    add_column :caches, :title, :string
  end
end
