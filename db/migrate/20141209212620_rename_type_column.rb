class RenameTypeColumn < ActiveRecord::Migration
  def up
    rename_column :messages, :type, :messageType 
  end

  def down
    rename_column :messages, :messageType, :type 
  end
end
