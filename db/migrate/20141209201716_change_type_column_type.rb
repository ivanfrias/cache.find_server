class ChangeTypeColumnType < ActiveRecord::Migration
  def up
    change_table :messages do |t|
      t.change :type, :string
    end 
  end

  def down
    change_table :messages do |t|
      t.change :type, :integer
    end 
  end
end
