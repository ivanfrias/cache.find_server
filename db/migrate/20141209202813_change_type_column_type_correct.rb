class ChangeTypeColumnTypeCorrect < ActiveRecord::Migration
  def up
    change_column(:messages, :type, :string)
  end

  def down
    change_column(:messages, :type, :integer)
  end
end
