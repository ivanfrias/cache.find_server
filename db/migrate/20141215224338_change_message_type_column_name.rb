class ChangeMessageTypeColumnName < ActiveRecord::Migration
  def up
    rename_column :messages, :messageType, :message_type 
  end

  def down
    rename_column :messages, :message_type, :messageType
  end
end
