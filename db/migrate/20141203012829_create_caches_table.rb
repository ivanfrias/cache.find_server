class CreateCachesTable < ActiveRecord::Migration
  def up
  	create_table :caches do |t|
  		t.string :latitude
      t.string :longitude 
      t.string :altitude
      t.string :description
      t.string :title
    end 
  end

  def down
      	drop_table :caches
  end
end