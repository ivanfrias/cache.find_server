require 'sinatra'
require 'sinatra/activerecord'
require './config/environments' #database configuration
require './models/cache'        #Cache class
require './models/message'      #Message class
require './models/location'     #Location class
require './models/region'       #Region
require 'json'

post '/cache' do
  request.body.rewind
  dt = JSON.parse(request.body.read.gsub("\"lon\": - ", "\"lon\": -"))
  
  for i in dt
    saveCache(i)
  end
  
  "Saved with success"
  
end

def saveCache(data)
  cache = Cache.new
  
  cache.name = data["name"]
  cache.description = data["description"]
  cache.status = data["status"]
  cache.terrain = data["terrain"]
  cache.oxcode = data["oxcode"]
  
  loc = Location.new
  loc.lat = data["location"]["lat"]
  loc.lon = data["location"]["lon"]
  
  region = Region.new
  region.state = data["region"]["state"]
  region.country = data["region"]["country"]
  region.county = data["region"]["county"]
  region.city = data["region"]["city"]
  region.postal = data["region"]["postal"]
  
  cache.location = loc
  cache.region = region
  
  cache.save
end

get '/caches' do
  content_type :json
  allCaches = Cache.all
  return generateCustomJson(allCaches)
end


get '/caches_of_state/:state' do
  content_type :json
  stateCaches = Cache.joins(:region).where(regions: { state: params[:state]})
  return generateCustomJson(stateCaches)
end

get '/caches_of_city/:city' do
  content_type :json
  cityCaches = Cache.joins(:region).where(regions: { city: params[:city]})
  return generateCustomJson(cityCaches)
end

def generateCustomJson(caches)
  iter = 0
  
  cacheJson = "["
  
  for i in caches
    cacheJson.concat("{\"name\": " + (i.name != nil ? "\"" + i.name.delete('"') + "\"" : "null") + ",")
    cacheJson.concat("\"location\": { \"lat\": " + ((i.location != nil && i.location.lat != nil) ? i.location.lat : "null" ) + ", \"lon\": " + ((i.location !=nil && i.location.lon != nil) ? i.location.lon : "null") + " },")
    cacheJson.concat("\"description\": " + (i.description != nil ? "\"" + i.description + "\"" : "null") + ",")
    cacheJson.concat("\"status\": " + (i.status != nil ? "\"" + i.status + "\"" : "null") + ",")
    cacheJson.concat("\"terrain\": " + (i.terrain != nil ? i.terrain : "null") + ",")
    cacheJson.concat("\"region\": { \"state\": " + (i.region != nil && i.region.state != nil ? "\"" + i.region.state + "\"" : "null") + ", \"country\": " + (i.region!=nil && i.region.country != nil ? "\"" + i.region.country + "\"" : "null") + ", \"county\": " + (i.region != nil && i.region.county != nil ? "\"" + i.region.county + "\"" : "null") + ", \"city\": " + (i.region != nil && i.region.city != nil ? "\"" + i.region.city + "\"" : "null") + ", \"postal\": " + (i.region != nil && i.region.postal != nil ? "\"" + i.region.postal + "\"" : "null") + "},")
    cacheJson.concat("\"oxcode\": " + (i.oxcode != nil ? "\"" + i.oxcode + "\"" : "null") + "}")
    
    iter += 1
    
    if iter < caches.size
      cacheJson.concat(",")
    end
      
  end
  
  cacheJson.concat("]")
  return cacheJson
end

get '/cache/:id' do
  queryCache(params[:id])
end

def queryCache(id)
  content_type :json
  cache = Cache.find(id)
  cache.to_json
rescue ActiveRecord::RecordNotFound
  "Cache with ID " + id + " Not Found"
end

put '/cache' do
  editCache(params)
end

def editCache(params)
  content_type :json
  cache = Cache.find(params[:id])
  cache.latitude = params[:latitude]
  cache.longitude = params[:longitude]
  cache.altitude = params[:altitude]
  cache.description = params[:description]
  cache.title = params[:title]
  if cache.save
    cache.to_json
  else
    "Sorry, there was an error!".to_json
  end
rescue ActiveRecord::RecordNotFound 
  "Cache with ID " + id + " Not Found" 
end

delete '/cache/:id' do
  content_type :json
  if Cache.destroy(params[:id])
    "Cache with ID " + params[:id] + " Deleted ".to_json
  else
    "Cache with ID " + params[:id] + " Not Deleted".to_json
  end
end

############################################ Messages #############################################################

post '/message' do
  content_type :json
  message = Message.new(params)
  message.message_type = '0'
  if message.save
    message.to_json
  else
    "Couldn't save Message".to_json
  end
end

post '/challenge' do
  content_type :json
  message = Message.new(params)
  message.message_type = '1'
  
  if message.save
    message.to_json
  else
    "Couldn't save Challenge Message".to_json
  end
end

get '/messages' do
  content_type :json
  Message.all.to_json
end

post '/messages_of_user' do
  content_type :json
  Message.where("destination = ? AND message_type = '0' AND created_at <= ?", params["destination"], params["date"]).order('created_at desc').limit(params["max"]).to_json
end

post '/challenges_of_user' do
  content_type :json
  return Message.where("destination = ? AND message_type = '1' AND created_at <= ?", params["destination"], params["date"]).order('created_at desc').limit(params["max"]).to_json
end 

post '/messages' do
  content_type :json
  messages = Message.where(messageType: '0')
  messages.to_json
end

get '/message/:id' do
  queryMessage(params[:id])
end

get '/challenge/:id' do
  queryChallenge(params[:id])
end

get '/challenges' do
  content_type :json
  message = Message.where(messageType: '1')
  message.to_json
end

def queryMessage(id)
  content_type :json
  message = Message.where(id: id, messageType: '0')
  message.to_json
rescue ActiveRecord::RecordNotFound
  "Message with ID " + id + " Not Found"
end

def queryChallenge(id)
  content_type :json
  message = Message.where(id: id, messageType: '1')
  message.to_json
rescue ActiveRecord::RecordNotFound
  "Challenge with ID " + id + " Not Found"
end

delete '/message/:id' do
  content_type :json
  if Message.destroy(params[:id])
    "Message with ID " + params[:id] + " Deleted ".to_json
  else
    "Message with ID " + params[:id] + " Not Deleted".to_json
  end
end

delete '/messages' do
  content_type :json
  if Message.all.destroy
    "All messages Deleted"
  else
    "Error deleting all messages"
  end
end


after do
  # Close the connection after the request is done so that we don't
  # deplete the ActiveRecord connection pool.
  ActiveRecord::Base.connection.close
end

