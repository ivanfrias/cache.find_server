class Cache < ActiveRecord::Base
  has_many :messages
  belongs_to :location
  belongs_to :region
end